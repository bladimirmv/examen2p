<?php
require_once "conexion.php";
require_once "methods.php";
session_start();


if ($_SESSION['type_person']=="student" || $_SESSION['type_person']=="") {
  header("Location:index.php");
}





?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <title>Document</title>
</head>
<body>

    <div class="container form col-12 ">

                  <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
                 <a href="#" class="imgadm navbar-brand  "><img src="admin.png" class=""></a>

                 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#uno">
                     <span class="navbar-toggler-icon"></span>  
                 </button>

                     <div class="collapse navbar-collapse" id="uno">

                     <ul >
                        <li><a href="">User: <strong><?php echo $_SESSION['first_name'].' '. $_SESSION['last_name'];?></strong> </a></li>
                        <li><a href="">Type: <strong><?php  echo $_SESSION['type_person'];?></strong> </a></li>             
                             <li><a href="desconect.php">Sign off </a></li>			 

                        </ul>
                
                      <ul class="nav nav-pills mb-3 " id="pills-tab" role="tablist">

                        
                        <li class="nav-item dropdown">
                                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" id="navbardrop">Subject</a>
                                    <div class="dropdown-menu">
                                  
                                        <a class="dropdown-item" data-toggle="pill" href="#pills-subject" role="tab" aria-controls="pills-subject">Add subject</a>
                                        <a class="dropdown-item" data-toggle="pill" href="#pills-question1" role="tab" aria-controls="pills-subject">Add complementation question</a>
                                        <a class="dropdown-item" data-toggle="pill" href="#pills-question2" role="tab" aria-controls="pills-subject">Add multiple selection question</a>
                                      
                                   
                                    </div>
                              </li>

                          <li class="nav-item">
                              <a class="nav-link" id="pills-admin-tab" data-toggle="pill" href="#pills-admin" role="tab" aria-controls="pills-admin" aria-selected="false">Add Administrator</a>
                          </li>        


                        </ul>
                      
		                      
                                
                                      
                            
                    </div>

            </nav>



    </div>   


    <div class="container form col-7 ">


<div class="tab-content" id="pills-tabContent">



<!--addsubject -->
  

  <div class="tab-pane fade " id="pills-subject" role="tabpanel" aria-labelledby="pills-subject-tab">  
              
           <br><br>
           <h1>Add Subject</h1>
           <br><br>


           <form action="processes_insert_Addsubject.php" method="post">

                              <label>Subject name</label>
                              <input type="text"name="txt_Subjectname"class="form-control"placeholder="Subject name...">

                              <label>Time</label>
                              <input type="text"name="txt_Time"class="form-control"placeholder="Time in minuts...">

                          
                              
                                
                                          <?php
                                            $obj = new methods();
                                            $sql="SELECT count(cod_subject) FROM ssubject";
                                            $data=$obj->cod_person_auto($sql); 
                                            foreach($data as $key){ 
                                            ?>                       

                                                <input type="text"name="txt_codsubject"class="inputtxt form-control" value="<?php echo $key+1;?>">

                                            <?php   
                                            }
                                            ?>
                              <input type="text"name="txt_codperson"class="inputtxt form-control" value="<?php echo $_SESSION['cod_person']; ?>">

                           <div class="container form col-3 float-right  ">
                           <input class="btnaddm btn btn-primary btn-block " type="submit" value="Save subject">  
                           </div>                                            
                                                                 

               </form>    

  </div>



<!--addquestion -->
<div class="tab-pane fade" id="pills-question1" role="tabpanel" aria-labelledby="pills-question1-tab">  
<br><br><br>
<form action="pag_question1.php" method="POST" class="form-inline">
<div class="container col-6">
<input type="text" name="txt_search" placeholder="search by name" class="form-control mr-sm-2">
  <button class="btn btn-success" type="submit">Search</button>
</div>
</form>
</div>

<div class="tab-pane fade" id="pills-question2" role="tabpanel" aria-labelledby="pills-question2-tab">  
<br><br><br>
<form action="pag_question2.php" method="POST" class="form-inline">
<div class="container col-6">
<input type="text" name="txt_search" placeholder="search by name" class="form-control mr-sm-2">
  <button class="btn btn-success" type="submit">Search</button>
</div>
</form>
</div>








<!-- addadmi -->


  <div class="tab-pane fade" id="pills-admin" role="tabpanel" aria-labelledby="pills-admin-tab">
  <form action="process_insert_administrator.php" method="post">
    <hr>
    <h1>Add Administrator</h1>                 
           
              <label>First name</label>
              <input type="text"name="txt_firstname"class="form-control"placeholder="First name...">
              
              <label>Last name</label>
              <input type="text"name="txt_lastname"class="form-control"placeholder="Last name...">

              <label>Job title</label>
              <input type="text"name="txt_jobtitle"class="form-control"placeholder="Job title...">

              <label>User name</label>
              <input type="text"name="txt_nusername"class="form-control"placeholder="User name...">

              <label>Password</label>
              <input type="password"name="txtnpasswd"class="form-control"placeholder="Password...">        

              <?php
              $obj = new methods();
              $sql="SELECT count(cod_person) FROM person";
              $data=$obj->cod_person_auto($sql); 
              foreach($data as $key){ 
              ?>                       

                  <input type="text"name="txt_cod"class="inputtxt form-control" value="<?php echo $key+1;?>">

              <?php     
              }

              ?>
              
              <div class="container form col-3  ">
              <input class="btnaddm btn btn-primary btn-block" type="submit" value="Save">  
              </div>
              <hr>

              </form>  
  
  </div>
  
</div>

    
   









  </div>





  

        



  <script src="js/jquery-3.3.1.slim.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script> 
</body>
</html>