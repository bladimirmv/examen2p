-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-05-2018 a las 11:12:31
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `evaluation_system`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrator`
--

CREATE TABLE `administrator` (
  `cod_administrator` int(11) NOT NULL,
  `job_title` varchar(50) NOT NULL,
  `cod_person` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrator`
--

INSERT INTO `administrator` (`cod_administrator`, `job_title`, `cod_person`) VALUES
(1, 'senior php', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluation_result`
--

CREATE TABLE `evaluation_result` (
  `cod_eresult` int(11) NOT NULL,
  `final_not` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE `person` (
  `cod_person` int(11) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `type_person` varchar(30) NOT NULL,
  `user_nam` varchar(50) NOT NULL,
  `passwd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `person`
--

INSERT INTO `person` (`cod_person`, `first_name`, `last_name`, `type_person`, `user_nam`, `passwd`) VALUES
(1, 'bladimir', 'medrano vargas', 'administrator', 'admin', 'admin'),
(2, 'mario', 'mamani mamani', 'student', 'mario66', 'nose');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `question1`
--

CREATE TABLE `question1` (
  `cod_question1` int(11) NOT NULL,
  `question_name` varchar(1000) NOT NULL,
  `type_question` varchar(30) NOT NULL,
  `answer` varchar(50) NOT NULL,
  `cod_subject` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `question1`
--

INSERT INTO `question1` (`cod_question1`, `question_name`, `type_question`, `answer`, `cod_subject`) VALUES
(1, 'PHP es un...', 'complementation', 'lenguaje de programacion', 1),
(2, 'SQLITE es un...', 'complementation', 'sistema de gestion de bases de datos', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `question2`
--

CREATE TABLE `question2` (
  `cod_question2` int(11) NOT NULL,
  `question_name` varchar(1000) NOT NULL,
  `type_question` varchar(30) NOT NULL,
  `answer` varchar(50) NOT NULL,
  `option1_name` varchar(50) NOT NULL,
  `option2_name` varchar(50) NOT NULL,
  `option3_name` varchar(50) NOT NULL,
  `cod_subject` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `question2`
--

INSERT INTO `question2` (`cod_question2`, `question_name`, `type_question`, `answer`, `option1_name`, `option2_name`, `option3_name`, `cod_subject`) VALUES
(1, 'Para que sirve php?', 'selection', ' para la generaciÃ³n de pÃ¡ginas web de forma dinÃ', 'solo para la generaciÃ³n de app para android ', ' para la generaciÃ³n de pÃ¡ginas web de forma dinÃ', ' para la generaciÃ³n de app de windows forms', 1),
(2, 'Para que nos sirve el SQLITE?', 'selection', 'Para la gestiÃ³n de bases de datos', 'Para programar', 'Para crear mapas', 'Para la gestiÃ³n de bases de datos', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ssubject`
--

CREATE TABLE `ssubject` (
  `cod_subject` int(11) NOT NULL,
  `subject_name` varchar(30) NOT NULL,
  `time_minuts` int(11) NOT NULL,
  `cod_administrator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ssubject`
--

INSERT INTO `ssubject` (`cod_subject`, `subject_name`, `time_minuts`, `cod_administrator`) VALUES
(1, 'php', 45, 1),
(2, 'sqlite', 30, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `student`
--

CREATE TABLE `student` (
  `cod_student` int(11) NOT NULL,
  `rude` int(11) NOT NULL,
  `cod_person` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `student`
--

INSERT INTO `student` (`cod_student`, `rude`, `cod_person`) VALUES
(1, 123568, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`cod_administrator`);

--
-- Indices de la tabla `evaluation_result`
--
ALTER TABLE `evaluation_result`
  ADD PRIMARY KEY (`cod_eresult`);

--
-- Indices de la tabla `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`cod_person`);

--
-- Indices de la tabla `question1`
--
ALTER TABLE `question1`
  ADD PRIMARY KEY (`cod_question1`);

--
-- Indices de la tabla `question2`
--
ALTER TABLE `question2`
  ADD PRIMARY KEY (`cod_question2`);

--
-- Indices de la tabla `ssubject`
--
ALTER TABLE `ssubject`
  ADD PRIMARY KEY (`cod_subject`);

--
-- Indices de la tabla `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`cod_student`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrator`
--
ALTER TABLE `administrator`
  MODIFY `cod_administrator` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `evaluation_result`
--
ALTER TABLE `evaluation_result`
  MODIFY `cod_eresult` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `question1`
--
ALTER TABLE `question1`
  MODIFY `cod_question1` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `question2`
--
ALTER TABLE `question2`
  MODIFY `cod_question2` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `student`
--
ALTER TABLE `student`
  MODIFY `cod_student` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
